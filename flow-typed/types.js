// @flow

export type Contact = {
    firstName: string,
    lastName: string,
    email: string,
    country: string,
    id?: number
};

declare type ActionType = 'ADD_CONTACTS' | 'ADD_CONTACT';

declare type ActionT<A: ActionType, P> = {|
    type: A,
    payload: P
|};

export type Action = ActionT<'ADD_CONTACTS', Contact[]> | ActionT<'ADD_CONTACT', Contact>;
