// @flow

import React, { Component } from 'react';
import { Route, BrowserRouter, Switch } from 'react-router-dom';
import type { Match, RouterHistory } from 'react-router-dom';
import { Provider } from 'react-redux';
import store from './store';
import Home from './Home/';
import AddContact from './AddContact/';
import ContactDetails from './ContactDetails/';

class App extends Component<{}> {
    render() {
        return (
            <Provider store={store}>
                <BrowserRouter>
                    <div className="app">
                        <Switch>
                            <Route exact path="/" component={Home} />
                            <Route path="/add" component={AddContact} />
                            <Route path="/details/:id" component={ContactDetails} />
                            <Route
                                path="/edit/:id"
                                component={(props: { match: Match, history: RouterHistory }) => (
                                    <AddContact {...props} />
                                )}
                            />
                            <Route component={() => <h1>404</h1>} />
                        </Switch>
                    </div>
                </BrowserRouter>
            </Provider>
        );
    }
}

export default App;
