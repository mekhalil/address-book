// @flow

import React, { Component } from 'react';
import dbStorage from '../dbHandler';
import countryList from 'country-list';
import NavigationBar from '../NavigationBar/';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import styled from 'styled-components';
import { getContact } from '../actionCreators';
import { connect } from 'react-redux';
import type { Match } from 'react-router-dom';

const countries = countryList();
const Container = styled.div`
    margin: 0 1em;
    padding: 1em;
`;

type Props = {
    match: Match,
    contact: Contact,
    getContactData: Function
};

class ContactDetails extends Component<Props> {
    componentDidMount() {
        this.props.getContactData();
    }

    render() {
        const country = this.props.contact.country
            ? countries.getName(this.props.contact.country)
            : '';
        return (
            <div>
                <NavigationBar
                    title={`${this.props.contact.firstName} ${this.props.contact.lastName}`}
                    displayEdit={true}
                    displayBack={true}
                    contactId={this.props.match.params.id ? this.props.match.params.id : ''}
                />
                <Container>
                    <Grid container spacing={24}>
                        <Grid item xs={12}>
                            <Typography variant="subheading" align="center">
                                Name: {this.props.contact.firstName} {this.props.contact.lastName}
                            </Typography>
                        </Grid>
                        <Grid item xs={12}>
                            <Typography variant="subheading" align="center">
                                Email: {this.props.contact.email}
                            </Typography>
                        </Grid>
                        <Grid item xs={12}>
                            <Typography variant="subheading" align="center">
                                Country: {country}
                            </Typography>
                        </Grid>
                    </Grid>
                </Container>
            </div>
        );
    }
}

const mapStateToProps = state => {
    const contactData = state.contact ? state.contact : {};
    return { contact: contactData };
};

const mapDispatchToProps = (dispatch: Function, ownProps) => ({
    getContactData() {
        dispatch(getContact(ownProps.match.params.id));
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(ContactDetails);
