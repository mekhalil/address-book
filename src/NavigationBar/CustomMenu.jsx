// @flow

import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

type Props = {
    anchorEl: any,
    handleClose: Function
};

class CustomMenu extends Component<Props> {
    render() {
        return (
            <div>
                <Menu
                    anchorEl={this.props.anchorEl}
                    open={Boolean(this.props.anchorEl)}
                    onClose={this.props.handleClose}
                >
                    <MenuItem
                        onClick={this.props.handleClose}
                        component={props => <Link to="/" {...props} />}
                    >
                        Contact List
                    </MenuItem>
                    <MenuItem
                        onClick={this.props.handleClose}
                        component={props => <Link to="/add" {...props} />}
                    >
                        Add a new Contact
                    </MenuItem>
                </Menu>
            </div>
        );
    }
}

export default CustomMenu;
