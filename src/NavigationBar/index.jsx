// @flow

import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import AddIcon from '@material-ui/icons/Add';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import BackIcon from '@material-ui/icons/ArrowBack';
import styled from 'styled-components';

import CustomMenu from './CustomMenu';

const ToolBarContainer = styled(Toolbar)`
    && {
        display: flex;
        justify-content: space-between;
        align-items: center;
    }
`;

type Props = {
    title: string,
    displayEdit?: boolean,
    displayRemove?: boolean,
    displayAdd?: boolean,
    displayBack?: boolean,
    contactId?: string
};

type State = {
    anchorEl: any
};

class NavigationBar extends Component<Props, State> {
    state = {
        anchorEl: null
    };

    openMenu = (event: any) => {
        this.setState({ anchorEl: event.currentTarget });
    };

    closeMenu = () => {
        this.setState({ anchorEl: null });
    };

    displayEditButton = () => {
        let { displayEdit, contactId } = this.props;
        if (displayEdit) {
            return (
                <IconButton
                    color="inherit"
                    component={props => (
                        <Link to={`/edit/${contactId ? contactId : ''}`} {...props} />
                    )}
                >
                    <EditIcon />
                </IconButton>
            );
        }
    };

    displayRemoveButton = () => {
        if (this.props.displayRemove) {
            return (
                <IconButton color="inherit" component={props => <Link to="/add" {...props} />}>
                    <DeleteIcon />
                </IconButton>
            );
        }
    };

    displayAddButton = () => {
        if (this.props.displayAdd) {
            return (
                <IconButton color="inherit" component={props => <Link to="/add" {...props} />}>
                    <AddIcon />
                </IconButton>
            );
        }
    };

    displayBackButton = () => {
        if (this.props.displayBack) {
            return (
                <IconButton color="inherit" component={props => <Link to="/" {...props} />}>
                    <BackIcon />
                </IconButton>
            );
        } else {
            return (
                <IconButton color="inherit" aria-label="Menu" onClick={this.openMenu}>
                    <MenuIcon />
                </IconButton>
            );
        }
    };

    render() {
        return (
            <AppBar position="static">
                <ToolBarContainer>
                    {this.displayBackButton()}
                    <Typography variant="title" color="inherit">
                        {this.props.title}
                    </Typography>
                    <div>
                        {this.displayEditButton()}
                        {this.displayRemoveButton()}
                        {this.displayAddButton()}
                    </div>
                </ToolBarContainer>
                <CustomMenu anchorEl={this.state.anchorEl} handleClose={this.closeMenu} />
            </AppBar>
        );
    }
}

export default NavigationBar;
