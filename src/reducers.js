// @flow

import { combineReducers } from 'redux';
import { ADD_CONTACTS, ADD_CONTACT } from './actions';

const contactList = (state = [], action: Action) => {
    if (action.type === ADD_CONTACTS) {
        return action.payload;
    }
    return state;
};

const contact = (state = {}, action: Action) => {
    if (action.type === ADD_CONTACT) {
        return action.payload;
    }
    return state;
};

const rootReducer = combineReducers({ contactList, contact });

export default rootReducer;
