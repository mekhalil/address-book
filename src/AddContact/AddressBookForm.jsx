// @flow

import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import './index.scss';

import countryList from 'country-list';
import { isValidEmail } from './validations';
const countries = countryList();
const countriesNameList = Object.keys(countries.getNameList());

type ContactField = {
    value: string,
    dirty: boolean,
    required: boolean
};

type ContactData = {
    firstName: ContactField,
    lastName: ContactField,
    email: ContactField,
    country: ContactField
};

type Props = {
    contact: ContactData,
    handleInputChange: Function
};

class AddressBookForm extends Component<Props> {
    validateTextField = (fieldData: ContactField): boolean =>
        fieldData.dirty && fieldData.required && !fieldData.value;

    validateEmailField = (fieldData: ContactField): boolean =>
        fieldData.dirty && fieldData.required && !isValidEmail(fieldData.value);

    render() {
        return (
            <Grid container spacing={24}>
                <Grid item xs={12} sm={6}>
                    <TextField
                        id="firstName"
                        name="firstName"
                        label="First Name"
                        value={this.props.contact.firstName.value}
                        onChange={this.props.handleInputChange}
                        error={this.validateTextField(this.props.contact.firstName)}
                        margin="normal"
                    />
                </Grid>
                <Grid item xs={12} sm={6}>
                    <TextField
                        id="lastName"
                        name="lastName"
                        label="Last Name"
                        value={this.props.contact.lastName.value}
                        onChange={this.props.handleInputChange}
                        error={this.validateTextField(this.props.contact.lastName)}
                        margin="normal"
                    />
                </Grid>
                <Grid item xs={12}>
                    <TextField
                        id="email"
                        name="email"
                        label="Email"
                        value={this.props.contact.email.value}
                        error={this.validateEmailField(this.props.contact.email)}
                        onChange={this.props.handleInputChange}
                        margin="normal"
                        fullWidth
                    />
                </Grid>
                <Grid item xs={12} sm={6}>
                    <TextField
                        id="country"
                        name="country"
                        select
                        label="Country"
                        value={this.props.contact.country.value}
                        onChange={this.props.handleInputChange}
                        error={this.validateTextField(this.props.contact.country)}
                        helperText="Please select your Country"
                        margin="normal"
                    >
                        {countriesNameList.map(country => (
                            <MenuItem
                                key={countries.getCode(country)}
                                value={countries.getCode(country)}
                            >
                                {country}
                            </MenuItem>
                        ))}
                    </TextField>
                </Grid>
            </Grid>
        );
    }
}

export default AddressBookForm;
