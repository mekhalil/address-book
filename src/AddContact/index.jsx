// @flow

import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { isValidEmail } from './validations';
import dbStorage from '../dbHandler';

import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import SaveIcon from '@material-ui/icons/Save';
import CancelIcon from '@material-ui/icons/Cancel';
import ErrorIcon from '@material-ui/icons/Error';
import Paper from '@material-ui/core/Paper';
import red from '@material-ui/core/colors/red';
import Tooltip from '@material-ui/core/Tooltip';
import styled from 'styled-components';
import faker from 'faker';

import AddressBookForm from './AddressBookForm';

import NavigationBar from '../NavigationBar/';
import type { Match, RouterHistory } from 'react-router-dom';

type ContactField = {
    value: string,
    dirty: boolean,
    required: boolean
};

type ContactData = {
    firstName: ContactField,
    lastName: ContactField,
    email: ContactField,
    country: ContactField
};

type State = {
    contact: ContactData,
    id: number | null,
    invalidFields: string[],
    showSubmitErrors: boolean
};

type Props = {
    match: Match,
    history: RouterHistory
};

const populateState = (contact?: Contact): State => ({
    contact: {
        firstName: {
            value: contact && contact.firstName ? contact.firstName : '',
            dirty: false,
            required: true
        },
        lastName: {
            value: contact && contact.lastName ? contact.lastName : '',
            dirty: false,
            required: true
        },
        email: {
            value: contact && contact.email ? contact.email : '',
            dirty: false,
            required: true
        },
        country: {
            value: contact && contact.country ? contact.country : '',
            dirty: false,
            required: true
        }
    },
    id: contact && contact.id ? contact.id : null,
    invalidFields: [],
    showSubmitErrors: false
});

const Container = styled.div`
    margin: 0 1em;
    padding: 1em;
`;

const CTAsWrapper = styled.div`
    display: flex;
    align-item: center;
`;

const SepartedButton = styled(Button)`
    && {
        margin: 1rem;
    }
`;

const PaperContainer = styled(Paper)`
    && {
        display: flex;
        align-item: center;
        padding: 1em;
        background-color: ${red[700]};
        color: white;
    }
`;

const TypographyCustom = styled(Typography)`
    && {
        color: white;
        margin-left: 1em;
    }
`;

class Edit extends Component<Props, State> {
    state = populateState();

    componentDidMount() {
        if (this.props.match.params.id) {
            dbStorage.get(parseInt(this.props.match.params.id)).then(val => {
                this.setState(populateState(val));
            });
        } else {
            const invalidFields = [];
            Object.keys(this.state.contact).forEach(key => {
                if (
                    this.state.contact[key] &&
                    typeof this.state.contact[key] === 'object' &&
                    this.state.contact[key].required &&
                    !this.state.contact[key].value
                ) {
                    invalidFields.push(key);
                }
            });
            this.setState({ invalidFields });
        }
    }

    handleInputChange = (event: any) => {
        if (this.state.showSubmitErrors) {
            this.setState({ showSubmitErrors: false });
        }

        this.setState({
            contact: {
                ...this.state.contact,
                [event.target.name]: {
                    value: event.target.value,
                    dirty: true,
                    required: this.state.contact[event.target.name].required
                }
            }
        });

        event.preventDefault();
    };

    storeContact = () => {
        if (!this.validateForm()) {
            return;
        }

        let contactData = {
            email: this.state.contact.email.value,
            firstName: this.state.contact.firstName.value,
            lastName: this.state.contact.lastName.value,
            country: this.state.contact.country.value
        };

        if (this.state.id) {
            contactData = Object.assign({}, contactData, { id: this.state.id });
            this.updateContact(contactData);
        } else {
            this.addContact(contactData);
        }
    };

    generateContact = () => {
        this.setState({
            contact: {
                firstName: {
                    value: faker.name.firstName(),
                    dirty: true,
                    required: true
                },
                lastName: {
                    value: faker.name.lastName(),
                    dirty: true,
                    required: true
                },
                email: {
                    value: faker.internet.email(),
                    dirty: true,
                    required: true
                },
                country: {
                    value: faker.address.countryCode(),
                    dirty: true,
                    required: true
                }
            }
        });
    };

    addContact = (contact: Contact) =>
        dbStorage.set(contact).then(() => this.props.history.push('/'));

    updateContact = (contact: Contact) =>
        dbStorage
            .set(contact)
            .then(() => this.props.history.push(`/details/${contact.id ? contact.id : ''}`));

    isValidField(name: string, contactField: ContactField): boolean {
        if (name === 'email') {
            return isValidEmail(contactField.value);
        } else {
            return !contactField.required || !!contactField.value;
        }
    }

    validateForm() {
        let isValid = true;
        let { contact } = this.state;

        for (let field in contact) {
            if (contact.hasOwnProperty(field)) {
                let fieldData: ContactField = contact[field];
                if (
                    (!fieldData.dirty && !fieldData.value) ||
                    !this.isValidField(field, fieldData)
                ) {
                    contact[field].dirty = true;
                    isValid = false;
                }
            }
        }

        if (!isValid) {
            this.setState({
                contact,
                showSubmitErrors: true
            });

            return false;
        }

        return true;
    }

    render() {
        return (
            <div>
                <NavigationBar title="New Contact" />
                <Container>
                    <PaperContainer
                        elevation={1}
                        className={`${
                            this.state.showSubmitErrors ? 'add-contact-show' : 'add-contact-hide'
                        } add-contact-form-error-alert`}
                    >
                        <ErrorIcon />
                        <TypographyCustom component="p">
                            Please, Try to fix the red fields first!
                        </TypographyCustom>
                    </PaperContainer>
                    <AddressBookForm
                        contact={{ ...this.state.contact }}
                        handleInputChange={this.handleInputChange}
                    />
                    <CTAsWrapper>
                        <SepartedButton
                            variant="fab"
                            color="primary"
                            aria-label="add"
                            onClick={this.storeContact}
                        >
                            <Tooltip title="Save">
                                <SaveIcon />
                            </Tooltip>
                        </SepartedButton>
                        <SepartedButton
                            variant="fab"
                            color="secondary"
                            aria-label="add"
                            component={props => <Link to="/" {...props} />}
                        >
                            <Tooltip title="Cancel">
                                <CancelIcon />
                            </Tooltip>
                        </SepartedButton>
                        <Tooltip title="Generate an user automatically">
                            <SepartedButton onClick={this.generateContact}>
                                Get a Fake contact
                            </SepartedButton>
                        </Tooltip>
                    </CTAsWrapper>
                </Container>
            </div>
        );
    }
}

export default Edit;
