// @flow

import idb from 'idb';
import type { UpgradeDB, DB } from 'idb';

class DBHandler {
    dbPromise: Promise<DB>;

    constructor() {
        if (!this.dbPromise) {
            this.dbPromise = idb.open('address-book-store', 1, (upgradeDB: UpgradeDB) => {
                upgradeDB.createObjectStore('contact', { keyPath: 'id', autoIncrement: true });
            });
        }
    }

    get(key: number) {
        return this.dbPromise.then(db => {
            return db
                .transaction('contact')
                .objectStore('contact')
                .get(key);
        });
    }

    getAll() {
        return this.dbPromise.then(db => {
            return db
                .transaction('contact')
                .objectStore('contact')
                .getAll();
        });
    }

    set(val: Contact) {
        return this.dbPromise.then(db => {
            const tx = db.transaction('contact', 'readwrite');
            tx.objectStore('contact').put(val);
            return tx.complete;
        });
    }

    update(key: number, val: Contact) {
        return this.dbPromise.then(db => {
            const tx = db.transaction('contact', 'readwrite');
            tx.objectStore('contact').put(val, key);
            return tx.complete;
        });
    }

    delete(key: number) {
        return this.dbPromise.then(db => {
            const tx = db.transaction('contact', 'readwrite');
            tx.objectStore('contact').delete(key);
            return tx.complete;
        });
    }

    clear() {
        return this.dbPromise.then(db => {
            const tx = db.transaction('contact', 'readwrite');
            tx.objectStore('contact').clear();
            return tx.complete;
        });
    }

    keys() {
        return this.dbPromise.then(db => {
            const tx = db.transaction('contact');
            const keys = [];
            const store = tx.objectStore('contact');

            // This would be store.getAllKeys(), but it isn't supported by Edge or Safari.
            // openKeyCursor isn't supported by Safari, so we fall back
            (store.iterateKeyCursor || store.iterateCursor).call(store, cursor => {
                if (!cursor) return;
                keys.push(cursor.key);
                cursor.continue();
            });

            return tx.complete.then(() => keys);
        });
    }
}

export default new DBHandler();
