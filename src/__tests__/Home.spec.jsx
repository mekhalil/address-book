// @flow

import React from 'react';
import { shallow } from 'enzyme';
import Home, { Unwrapped as UnwrappedHome } from '../Home/';
import ContactInfo from '../Home/ContactInfo';
import faker from 'faker';

const fakeUsers = [];

for (let i = 0; i < 10; i++) {
    fakeUsers.push({
        firstName: faker.name.firstName(),
        lastName: faker.name.lastName(),
        email: faker.internet.email(),
        country: faker.address.countryCode()
    });
}

function getContactListData() {
    return fakeUsers;
}

test('render Home without crashing', () => {
    shallow(<UnwrappedHome contactList={fakeUsers} getContactListData={getContactListData} />);
});

test('Number of Components', () => {
    const component = shallow(
        <UnwrappedHome contactList={fakeUsers} getContactListData={getContactListData} />
    );
    expect(component.find(ContactInfo).length).toEqual(fakeUsers.length);
});
