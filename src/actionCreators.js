// @flow

import { ADD_CONTACTS, ADD_CONTACT } from './actions';
import dbStorage from './dbHandler';

export function addContacts(contactList: Contact[]) {
    return { type: ADD_CONTACTS, payload: contactList };
}

export function getContactList() {
    return (dispatch: Function) => {
        dbStorage
            .getAll()
            .then(contactList => {
                dispatch(addContacts(contactList));
            })
            .catch(error => {
                console.error('Cannot get the data from indexedDB', error); // eslint-disable-line no-console
            });
    };
}

export function addContact(contact: Contact) {
    return { type: ADD_CONTACT, payload: contact };
}

export function getContact(contactId: string) {
    return (dispatch: Function) => {
        dbStorage
            .get(parseInt(contactId))
            .then(contact => {
                dispatch(addContact(contact));
            })
            .catch(error => {
                console.error('Cannot get the data from indexedDB', error); // eslint-disable-line no-console
            });
    };
}
