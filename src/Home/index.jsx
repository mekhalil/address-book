// @flow

import React, { Component } from 'react';

import List from '@material-ui/core/List';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { getContactList } from '../actionCreators';

import ContactInfo from './ContactInfo';
import NavigationBar from '../NavigationBar/';

import dbStorage from '../dbHandler';

type Props = {
    contactList: Contact[],
    getContactListData: Function
};

const Container = styled.div`
    margin: 1em 0;
    padding: 1em;
`;

class Home extends Component<Props> {
    componentDidMount() {
        this.props.getContactListData();
    }

    deleteContact(id: number) {
        dbStorage.delete(id).then(() => {
            this.props.getContactListData();
        });
    }

    displayContactList() {
        const {contactList} = this.props;
        if (contactList && contactList.length) {
            return contactList.map(contact => (
                <ContactInfo
                    {...contact}
                    key={contact.id}
                    deleteContact={() => this.deleteContact(parseInt(contact.id))}
                />
            ));
        } else {
            return (
                <Typography variant="title" gutterBottom align="center">
                    Empty Contact List
                </Typography>
            );
        }
    }

    render() {
        return (
            <div>
                <NavigationBar title="Address Book" displayAdd={true} />
                <Container>
                    <Grid container spacing={24} />
                    <List dense={false}>{this.displayContactList()}</List>
                </Container>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return { contactList: state.contactList };
};

const mapDispatchToProps = (dispatch: Function) => ({
    getContactListData() {
        dispatch(getContactList());
    }
});

// This is for testing only
export const Unwrapped = Home;

export default connect(mapStateToProps, mapDispatchToProps)(Home);
