// @flow

import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import UserIcon from '@material-ui/icons/Person';
import { Link } from 'react-router-dom';
import Divider from '@material-ui/core/Divider';

type Props = {
    ...Contact,
    deleteContact: Function
};

class ContactInfo extends React.Component<Props> {
    linkToDetails = (props: any) => (
        <Link to={`/details/${this.props.id ? this.props.id : ''}`} {...props} />
    );

    render() {
        let { firstName, lastName, email, deleteContact } = this.props;
        return (
            <div>
                <ListItem component={this.linkToDetails}>
                    <ListItemAvatar>
                        <Avatar>
                            <UserIcon />
                        </Avatar>
                    </ListItemAvatar>
                    <ListItemText
                        primary={`${firstName ? firstName : ''} ${lastName ? lastName : ''}`}
                        secondary={`${email ? email : ''}`}
                    />
                    <ListItemSecondaryAction>
                        <IconButton aria-label="Delete" onClick={deleteContact}>
                            <DeleteIcon />
                        </IconButton>
                    </ListItemSecondaryAction>
                </ListItem>
                <Divider />
            </div>
        );
    }
}

export default ContactInfo;
